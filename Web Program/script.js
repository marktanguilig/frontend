var book=[{"name":"Manuscript Found in Accra"},{"name":"Fifth Mountain"},{"name":"Inspirations"},
		{"name":"The Winner Stands Alone"},{"name":"Warrior of the Light"},{"name":"Safe Haven"},{"name":"The Best of Me"},
		{"name":"A Walk to Remember"},{"name":"Nights in Rodanthe"},{"name":"Message in a Bottle"}];
var bookimages=[{"img":"images/manuscript.jpg width=140 height=200"},{"img":"images/fifthmountain.png width=150 height=200"},{"img":"images/inspirations.png width=150 height=200"},
				{"img":"images/thewinner.jpg width=150 height=200"},{"img":"images/warrior.png width=150 height=200"},{"img":"images/safehaven.jpg width=150 height=200"},
				{"img":"images/bestofme.jpg width=150 height=200"},{"img":"images/awalk.jpg width=150 height=200"},{"img":"images/nightsin.jpg width=150 height=200"},
				{"img":"images/messageinabottle.jpg width=150 height=200"}];
var bookprice=[{"price":275.00},{"price":388.89},{"price":500.09},{"price":416.59},{"price":311.19},{"price":549.00},{"price":549.00},
				{"price":315.00},{"price":315.00},{"price":315.00}];
var bookinfo=[{"info":"Jerusalem, 1099. The city awaits the invasion of the crusaders who have surrounded its walls. There, inside the ancient gates, women and men of every age and faith have gathered to hear the wisdom of a mysterious man known only as the Copt.  As the wise man speaks of loyalty, fear, bravery and solitude, of love, sex, beauty and elegance, his words reveal the human values that have endured throughout time�then as now, these words make known who we are, what we fear and what we hope for the future."},
			{"info":"In the ninth century b.c., the Phoenician princess Jezebel orders the execution of all the prophets who refuse to worship the pagan god Baal. Commanded by an angel of God to flee Israel, Elijah seeks safety in the land of Zarephath, where he unexpectedly finds true love with a young widow. But this newfound rapture is to be cut short, and Elijah sees all of his hopes and dreams irrevocably erased as he is swept into a whirlwind of events that threatens his very existence."},
			{"info":"'Anthology' comes from the Greek word that stands for garlands - a bouquet of flowers. An anthology then, should be a sort of reminder of something else, a small token of something much larger. In the case of flowers, they bring with their fragrance and colorfulness the reminder of the fields, of a season. Coelho's anthology, therefore, is not only a collection of texts or poems, but a gift, something arranged according to his sensitivities, to give to others. The selection of books presented in this volume have been chosen as if from a vast field of flowers, stretching infinitely into time's horizon. Coelho's selection is ordered in to the four elements, symbolizing both our world on all its directions, and the way we dwell in this world, the way we say it. In 'Earth' we find writers as diverse as Oscar Wilde and D H Lawrence; in 'Air' Nelson Mandela and Gabriel Garcia Marques; in 'Fire' Rumi and Mary Shelley; in 'Water' Hans Christian Anderson and Machiavelli."},
			{"info":"A profound meditation on personal power and innocent dreams that are manipulated or undone by success, The Winner Stands Alone is set in the exciting worlds of fashion and cinema. Taking place over the course of twenty-four hours during the Cannes Film Festival, it is the story of Igor, a successful, driven Russian entrepreneur who will go to the darkest lengths to reclaim a lost love�his ex-wife, Ewa. Believing that his life with Ewa was divinely ordained, Igor once told her that he would destroy whole worlds to get her back. The conflict between an individual evil force and society emerges, and as the novel unfolds, morality is derailed."},
			{"info":"Warrior of the Light: A Manual is an inspirational companion to The Alchemist, an international bestseller that has beguiled millions of readers around the world. Every short passage invites us to live out our dreams, to embrace the uncertainty of life, and to rise to our own unique destiny. In his inimitable style, Paulo Coelho helps bring out the Warrior of the Light within each of us. He also shows readers how to embark upon the way of the Warrior: the one who appreciates the miracle of being alive, the one who accepts failure, and the one whose quest leads him to become the person he wants to be."},
			{"info":"When a mysterious young woman named Katie appears in the small North Carolina town of Southport, her sudden arrival raises questions about her past. Beautiful yet self-effacing, Katie seems determined to avoid forming personal ties until a series of events draws her into two reluctant relationships: one with Alex, a widowed store owner with a kind heart and two young children; and another with her plainspoken single neighbor, Jo. Despite her reservations, Katie slowly begins to let down her guard, putting down roots in the close-knit community and becoming increasingly attached to Alex and his family."},
			{"info":"In the spring of 1984, high school students Amanda Collier and Dawson Cole fell deeply, irrevocably in love. Though they were from opposite sides of the tracks, their love for one another seemed to defy the realities of life in the small town of Oriental, North Carolina. But as the summer of their senior year came to a close, unforeseen events would tear the young couple apart, setting them on radically divergent paths."},
			{"info":"In the prologue to his latest novel, Nicholas Sparks makes the rather presumptuous pledge first you will smile, and then you will cry, but sure enough, he delivers the goods. With his calculated ability to throw your heart around like a yo-yo (try out his earlier Message in the Bottle or The Notebook if you really want to stick it to yourself), Sparks pulls us back to the perfect innocence of a first love.In 1958 Landon Carter is a shallow but well-meaning teenager who spends most of his time hanging out with his friends and trying hard to ignore the impending responsibilities of adulthood. Then Landon gets roped into acting the lead in the Christmas play opposite the most renowned goody two-shoes intown: Jamie Sullivan. Against his best intentions and the taunts of his buddies, Landon finds himself falling for Jamie and learning some central lessons in life.Like John Irving's A Prayer for Owen Meany, Sparks maintains a delicate and rarely seen balance of humor and sentiment. While the plot may not be the most original, this boy-makes-good tearjerker will certainly reel in the fans. Look for a movie starring beautiful people or, better yet, snuggle under the covers with your tissues nearby and let your inner sap run wild."},
			{"info":"At forty-five, Adrienne Willis must rethink her entire life when her husband abandons her for a younger woman. Reeling with heartache and in search of a respite, she flees to the small coastal town of Rodanthe, North Carolina, to tend to a friend's inn for the weekend. But when a major storm starts moving in, it appears that Adrienne's perfect getaway will be ruined�until a guest named Paul Flanner arrives. At fifty-four, Paul has just sold his medical practice and come to Rodanthe to escape his own shattered past. Now, with the storm closing in, two wounded people will turn to each other for comfort�and in one weekend set in motion feelings that will resonate throughout the rest of their lives."},
			{"info":"The book is a heart-wrenching tale of self-discovery, renewal, and the courage it takes to love again. Teresa Osborne, a 36-year-old single mother, finds a bottle washed up on a Cape Cod beach. The scrolled-up message inside is a passionate love letter written by a heartbroken man named Garrett who is grieving over 'his darling Catherine.' Teresa is so moved by the stranger's poignant words that she vows to find the penman and publishes the letter in her syndicated Boston newspaper column. Questions linger in her mind and heart: Who is Garrett? Who is Catherine? What is their story? And most importantly, why did this bottle find its way to her? Imagining that Garrett is the type of man she has always been seeking, Teresa sets out on an impulsive, hope-filled search. Her journey, her discovery, and the wisdom gained from this voyage of self-discovery changes her life forever. Love's unimaginable strength as well as its tremendous fragility echoes on each page of Sparks's newest gem."}];
			
var bookstock=[{"stock":5},{"stock":6},{"stock":20},{"stock":10},{"stock":15},{"stock":50},{"stock":50},{"stock":40},{"stock":20},{"stock":30}];
var cartId;
var items;
var purchaseitems;
var purchaseprice=0;


var storevalues=0;
localStorage.setItem("fund", 3569);


localStorage.setItem("book",JSON.stringify(book));
localStorage.setItem("bookimages",JSON.stringify(bookimages));
localStorage.setItem("bookprice",JSON.stringify(bookprice));

var getpurchase = localStorage.getItem("purchase");
var purchasecheck = JSON.parse(getpurchase);
var retrievedData = localStorage.getItem("name");
var itemcheck = JSON.parse(retrievedData);
function showContent(){
	
	
	var book=localStorage.getItem("book");
	var bookimages=localStorage.getItem("bookimages");
	var bookprice=localStorage.getItem("bookprice");
	
	book=JSON.parse(book);
	bookimages=JSON.parse(bookimages);
	bookprice=JSON.parse(bookprice);
	
	document.getElementById("carttotal").innerHTML="Cart Total: P 0.00";
	document.getElementById('btncheckout').style.visibility = 'hidden';
	var table=document.getElementById("tblImage");
	var counter=0;
	var row=table.insertRow(counter);
	var itemcount=0;
	
	for(var i=0; i<document.getElementById("tblImage").rows[counter];i++){
		document.getElementById("tblImage").rows[counter].deleteCell(i);
	}
	
	for (var i = document.getElementById("tblImage").rows.length; i > 1; i--) {
		document.getElementById("tblImage").deleteRow(i - 1);
	}
	
	for(var i=0;i<book.length;i++){
		var objvalue=book[i].name;
		var objbookprice=bookprice[i].price;
		var txtvalue=document.getElementById("txtid").value.toLowerCase();
		if(objvalue.toLowerCase().indexOf(txtvalue)!==-1){
			if(itemcount==5){
				counter+=1;
				row=table.insertRow(counter);
			}
			var cell1=row.insertCell(0);
			cell1.innerHTML="<center><img style='border-radius: 25px;border: 2px solid;' src=" + bookimages[i].img + " >" + "<br>" + objvalue + "<br/> " + "P " + objbookprice + " <br> In Stock: " + bookstock[i].stock +"  </center> "  + "<input type='submit'  class='btnposition' value='+'  id=btnid" + i + " onclick='buttonclickevent(btnid" + i + ")' />"; 
			itemcount+=1;
			document.getElementById("result").innerHTML= itemcount + " Results";
		}
		else{
			document.getElementById("result").innerHTML= itemcount + " Results";
		}
	}
	sessionStorage.removeItem("fund");
	
}
var cartprice = 0;
var itemCount=0;
function buttonclickevent(val){
	var id=val.id;
	
	if (document.getElementById(id).value=='+'){
		var thiscarttbl=document.getElementById("tblcart");
		var row=thiscarttbl.insertRow(0);
		for(var x=0;x<book.length;x++){
			var compID="btnid" + x;
			if(compID==id){			
				var cell=row.insertCell(0);
				cell.innerHTML=" <img style='float:left; border: 2px solid;border-radius: 25px;' id='chk" 
				+ x + "' src=" + bookimages[x].img + " ></br>" + book[x].name + "<br> P " + bookprice[x].price +
				"<br><br><br><br><br><br><br> <a href=# onclick='removeFromCart(this.parentNode.parentNode.rowIndex)'>Remove from Cart</a> <p name='additem' id='" + id + "'> </p>";
				cartprice += parseFloat(bookprice[x].price);
				document.getElementById("carttotal").innerHTML="Cart Total: P " + cartprice.toFixed(2);
				itemCount+=1;
				document.getElementById("cartcount").innerHTML="Your Cart (" + itemCount +" Item/s)";
				document.getElementById('btncheckout').style.visibility = 'visible';
				document.getElementById(compID).value='-';
			}
		}

	}else if (document.getElementById(id).value=='-'){
		document.getElementById(id).value='+';
		id="";
	}	
}

function removeFromCart(x){
		var y=document.getElementsByName('additem');
		var z=y[x].id;
		document.getElementById(y[x].id).value="+";
		itemCount-=1;
		cartprice -= parseFloat(bookprice[z.substring(5,6)].price);
		document.getElementById("carttotal").innerHTML="Cart Total: P " + cartprice.toFixed(2);
		document.getElementById("tblcart").deleteRow(x);
		document.getElementById("cartcount").innerHTML="Your Cart (" + (document.getElementById("tblcart").rows.length-1) +" Item/s)";
		var table = document.getElementById('tblcart');
		
		if ((document.getElementById("tblcart").rows.length-1)==0){
			document.getElementById('btncheckout').style.visibility = 'hidden';
		}
}
function check(){
	 var rows = document.getElementById('tblcart')[0];
}
function checkitems(){
	var y=document.getElementsByName('additem');
	var x =document.getElementById('tblcart');
	items=new Array();
	
	for(var i=0;i<x.rows.length-1;i++){
		items.push(y[i].id);	
	}
	localStorage.setItem("name", JSON.stringify(items));
	window.location="cart.html";
}
function getitems(){
	var table=document.getElementById("tbldisplaycart");
	document.getElementById("mainfunds2").innerHTML=sessionStorage.getItem("fund");
	for(var i=0;i<itemcheck.length;i++){
		var row=table.insertRow(0);
		var cell=row.insertCell(0);
		var cell2=row.insertCell(1);
		var cell3=row.insertCell(2);
		var cartid=itemcheck[i].substring(5,6);
		cell.innerHTML="<img style='float:left; border: 2px solid;border-radius: 25px;' src=" + bookimages[cartid].img + " /> <p name='purchaseitem' id='" + cartid + "'> </p>";
		cell2.innerHTML=book[cartid].name + "</br> P" + bookprice[cartid].price + "</br></br>" + bookinfo[cartid].info + "<br> <div align='right'> <a href=# onclick='removeItemFromCart(this.parentNode.parentNode.parentNode.rowIndex)'>Remove from Cart</a> </div> ";
	}
}
function removeItemFromCart(x){
	document.getElementById("tbldisplaycart").deleteRow(x);
	items.remove("btnid" + x);
}
function gotopurchase(){
	var y=document.getElementsByName('purchaseitem');
	var x =document.getElementById('tbldisplaycart');
	purchaseitems=new Array();
	for(var i=0;i<x.rows.length;i++){
		purchaseitems.push(y[i].id);
	}
	localStorage.setItem("purchase", JSON.stringify(purchaseitems));
	window.location="receipt.html";
}
function loadpurchase(){
	var table=document.getElementById("tblpurchase");
	document.getElementById("purchasecount").innerHTML="Total Items: " + purchasecheck.length;
	for(var i=0;i<purchasecheck.length;i++){
		var row=table.insertRow(0);
		var cell1=row.insertCell(0);
		var cell2=row.insertCell(1);
		cell1.innerHTML=book[purchasecheck[i]].name;
		cell2.innerHTML="<p style='float:right'> P " + parseFloat(bookprice[purchasecheck[i]].price) + "</p>";
		purchaseprice+=bookprice[purchasecheck[i]].price;
	}
	document.getElementById("totalpurchase").innerHTML="Total: P" + purchaseprice;
	localStorage.setItem("purchasetotal", purchaseprice);
}

function gotomain(){	
	
	window.location="main2.html";
}
